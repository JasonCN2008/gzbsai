package com.spring.cache;

import com.example.cache.SimpleCache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SimpleCacheTests {
    @Autowired
    private SimpleCache simpleCache;

    @Test
    void testSimpleCache(){
        System.out.println(simpleCache);
    }
}
