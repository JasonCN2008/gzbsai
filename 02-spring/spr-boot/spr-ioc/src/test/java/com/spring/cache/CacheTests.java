package com.spring.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class CacheTests {
    /**
     * 这里的含义，从spring容器中查找类型为Cache，名字为weakCache对象，
     * 然后为cache属性赋值。
     */
    @Autowired  //byType->byName
    @Qualifier("weakCache")
    private Cache cache;

    /**
     * @Resource 描述属性时表示先按指定名字去查找对应的Bean对象，然后进行依赖注入。
     */
    @Resource(name="weakCache")//byName->byType
    //private Cache cache;

    @Test
    void testCache(){
        System.out.println(cache);
    }
}
