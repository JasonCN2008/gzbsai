package com.spring.cache;

import com.example.cache.LogCache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogCacheTests {

    @Autowired
    private LogCache logCache;

    @Test
    void testLogCache(){
        System.out.println(logCache);
    }
}
