package com.spring;

import com.example.cache.LogCache;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * 定义SpringBoot项目启动的入口
 * @SpringBootApplication 描述的类为springboot工程启动的入口。
 * 思考：
 * 1)Spring Framework是什么?(框架-资源整合框架)
 * 2)请问何为资源?(程序中的资源就是对象，例如连接池，线程池，三方类库)
 * 3)如何快速整合这些资源？基于SpringBoot脚手架
 * 4)如何快速找到这些资源?默认从启动类所在包或子包去查找
 */

@Import(LogCache.class)
@SpringBootApplication
public class BootIocApplication {
    public static void main(String[] args) {
        SpringApplication.run(BootIocApplication.class, args);
    }
}
