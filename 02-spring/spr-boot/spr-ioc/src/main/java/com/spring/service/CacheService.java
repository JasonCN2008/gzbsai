package com.spring.service;

import com.spring.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    private Cache cache;

    @Autowired //假如只有这个一个构造函数,@Autowired注解可以省略
    public CacheService(@Qualifier("weakCache") Cache cache){
        this.cache=cache;
        System.out.println("CacheService(@Qualifier(\"weakCache\") Cache cache)");
    }

}
