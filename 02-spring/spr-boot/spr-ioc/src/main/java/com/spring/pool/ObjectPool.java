package com.spring.pool;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Scope("prototype") //每次从spring获取对象，都是创建新的对象，这个对象不会存储到池中，并且默认是延迟加载
@Scope("singleton") //定义单例作用域，类型相同，名字也相同的类的实例在内存中只能有一份。
@Lazy //此注解描述的类会延迟对象的创建，何时需要何时创建。
@Component
public class ObjectPool {//一般可以将池理解为一个相对比较大的对象
    //private byte[]data = new byte[1024*1024*100];
    public ObjectPool(){
        System.out.println("ObjectPool()");
    }

    /**
     * @PostConstruct 注解描述的方法为对象的生命周期初始化方法，
     * 这个方法会在对象构建完以后执行，也就构造方法执行之后，
     * 执行此方法。
     */
    @PostConstruct
    public void init(){
        System.out.println("init()");
    }

    /**
     *  @PreDestroy描述的方法为对象生命周期销毁方法，在对象销毁之前，
     *  会执行此方法。但前提是这个对象需要是单例作用域。
     */
    @PreDestroy
    public void destroy(){
        System.out.println("destroy()");
    }
}
