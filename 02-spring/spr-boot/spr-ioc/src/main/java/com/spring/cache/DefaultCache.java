package com.spring.cache;
import org.springframework.stereotype.Component;
/**
 * 在springboot启动类所在包或子包定义如下类
 * 并通过@Component注解进行描述，进而将类交给
 * spring管理。
 */
@Component("defaultCache1")
//@Component("defaultCacheBean")
public class DefaultCache {//可以将此类理解为简单的缓存组件
    public DefaultCache(){
        System.out.println("DefaultCache()");
    }
}//IOC
