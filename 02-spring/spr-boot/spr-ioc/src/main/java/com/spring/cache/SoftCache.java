package com.spring.cache;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//@Primary //设置此对象的优先级(假如其它对象中依赖Cache类型的对象，则优先注入@Primary注解描述的类型)
@Component
public class SoftCache implements Cache{
    public SoftCache(){
        System.out.println("SoftCache()");
    }
}
