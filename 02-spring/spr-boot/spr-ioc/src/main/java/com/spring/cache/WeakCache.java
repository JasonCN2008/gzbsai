package com.spring.cache;

import org.springframework.stereotype.Component;

@Component
public class WeakCache implements Cache{
    public WeakCache(){
        System.out.println("WeakCache()");
    }
}
