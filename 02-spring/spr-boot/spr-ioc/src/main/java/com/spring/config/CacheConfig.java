package com.spring.config;

import com.example.cache.SimpleCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Configuration 注解描述的类，一般为spring的配置类，
 * 在类中可以定义一些bean对象的配置，尤其是第三方bean。
 */
//@Component
@Configuration
public class CacheConfig {
    /**
     * @Bean 注解描述的方法一般会写在@Configuration描述的类中。
     * 这个方法会由spring通过反射技术进行调用，方法的返回值(Bean对象)要
     * 交给spring管理，这个bean的名字默认为方法名，当然也可以自己起名字
     * @return
     */
    @Bean
    //@Bean("sCache") 通过@Bean注解定义bean名字
    public SimpleCache simpleCache(){
        SimpleCache simpleCache=new SimpleCache();
        simpleCache.init();
        return simpleCache;
    }

    @Bean
    public Object simpleObject(){
        Object obj=new Object();
        simpleCache();
        return obj;
    }
}
