package com.spring.mvc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@RequestMapping("/file/")
public class FileController {
    @Value("${jy.file.path}")
    private String resourcePath;//="d:/uploads/";
    @Value("${jy.file.host}")
    private String resourceHost;//="http://localhost:8881/";

    @PostMapping("/upload/") //核心业务->文件上传
    public String uploadFile(MultipartFile uploadFile) throws IOException {

        //1.创建文件存储目录(按时间创建-yyyy/MM/dd)
        //1.1获取当前时间的一个目录
        String dateDir = DateTimeFormatter.ofPattern("yyyy/MM/dd")
                .format(LocalDate.now());
        //1.2构建目录文件对象
        File uploadFileDir=new File(resourcePath,dateDir);
        if(!uploadFileDir.exists())uploadFileDir.mkdirs();
        //2.给文件起个名字(尽量不重复)
        //2.1获取原文件后缀
        String originalFilename=uploadFile.getOriginalFilename();
        String ext = originalFilename.substring(originalFilename.lastIndexOf("."));
        //2.2构建新的文件名
        String newFilePrefix= UUID.randomUUID().toString();
        String newFileName=newFilePrefix+ext;
        //3.开始实现文件上传
        //3.1构建新的文件对象,指向实际上传的文件最终地址
        File file=new File(uploadFileDir,newFileName);
        //3.2上传文件(向指定服务位置写文件数据)
        uploadFile.transferTo(file);
        String fileRealPath=resourceHost+dateDir+"/"+newFileName;
        //后续可以将上传的文件信息写入到数据库?
        return fileRealPath;
    }
}
