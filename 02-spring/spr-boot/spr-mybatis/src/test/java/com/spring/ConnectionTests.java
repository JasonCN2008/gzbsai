package com.spring;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;

@SpringBootTest
public class ConnectionTests {
    /**
     * SqlSession是mybatis中提供的与数据库进行会话的对象，
     * 这个对象系统启动时会为我们创建。
     */
    @Autowired
    private SqlSession sqlSession;
    @Test
    void testGetConnection(){
        Connection connection = sqlSession.getConnection();
        System.out.println(connection);
    }
}
