package com.spring;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SqlSessionTests {
    @Autowired
    private SqlSession sqlSession;
    @Test
    void testQuery01(){
        String statement="a.b.selectNotice";
        Object o = sqlSession.selectOne(statement);
        System.out.println(o);
    }
}
