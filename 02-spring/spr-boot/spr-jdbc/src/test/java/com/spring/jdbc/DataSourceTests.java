package com.spring.jdbc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class DataSourceTests {
    /**
     * DataSource 是java中连接数据库的一个数据源规范，我们获取与数据库的连接时，
     * 可以调用此对象的getConnection()方法(这个方法的内部一般是先从连接池获取
     * 连接，假如池中没有连接，则基于驱动建立连接，并且将连接放入池中)
     *
     * FAQ?
     * 1)这里DataSource属性的值，运行时具体指的是谁？HikariCPDataSource
     * 2)HikariCPDataSource对象是什么时候创建的？(服务启动时创建)
     */
    @Autowired
    private DataSource dataSource;

    @Test
    void testGetConnection() throws SQLException {
       Connection conn = dataSource.getConnection();//从连接池获取连接
       System.out.println(conn);
       conn.close();//将连接还回池中
    }

}
