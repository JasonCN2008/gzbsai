package com.spring.jdbc;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringBootTest
public class JdbcTests {
    @Autowired
    private DataSource dataSource;

    /**
     * 基于id查询数据库中的数据，并将其封装到map。
     * 思考：
     * 1)基于jdbc实现查询的基本步骤
     * 2)使用连接池时，涉及的一些设计模式？
     * @throws SQLException
     */
    @Test
    void testQuery01() throws SQLException {
        //1.获取连接(这里的链接来自连接池)
        Connection conn=dataSource.getConnection();
        //2.创建Statement(基于此对象发送sql)
        String sql="select id,title,type,content,status,user_id,created_time " +
                   " from tb_sys_notices " +
                   " where id=?";//?表示占位符
        PreparedStatement pstmt=conn.prepareStatement(sql);
        pstmt.setInt(1,1);
        //3.发送SQL
        ResultSet rs = pstmt.executeQuery();
        //4.处理查询结果
        while(rs.next()){//循环一次取一行，取到的数据封装到map
            Map<String,Object> map=new HashMap();
            map.put("id",rs.getInt("id"));
            map.put("title",rs.getString("title"));
            map.put("type",rs.getString("type"));
            map.put("content",rs.getString("content"));
            map.put("status",rs.getString("status"));
            map.put("user_id",rs.getInt("user_id"));
            map.put("created_time",rs.getTimestamp("created_time"));
            System.out.println(map);
        }
        //5.释放资源(原则事先创建的后关闭)
        rs.close();
        pstmt.close();
        conn.close();//这里的链接不是真正的关闭，而是连接换回池中。
    }

    @Test
    void testQuery02() throws SQLException {
        //1.获取连接(这里的链接来自连接池)
        Connection conn=dataSource.getConnection();
        //2.创建Statement(基于此对象发送sql)
        String sql="select id,title,type,content,status,user_id userId,created_time createdTime " +
                   " from tb_sys_notices " +
                   " where id=?";//?表示占位符
        PreparedStatement pstmt=conn.prepareStatement(sql);
        pstmt.setInt(1,1);
        //3.发送SQL
        ResultSet rs = pstmt.executeQuery();
        ResultSetMetaData metaData = rs.getMetaData();//获取结果集中的元数据
        int columnCount = metaData.getColumnCount();//获取select列表中列的个数
        //4.处理查询结果
        while(rs.next()){//循环一次取一行，取到的数据封装到map
            Map<String,Object> map=new HashMap();
            for(int i=1;i<=columnCount;i++){//循环所有列，并且取出每列值，并封装到map。
                map.put(metaData.getColumnLabel(i),
                        rs.getObject(metaData.getColumnLabel(i)));
            }
            System.out.println(map);
        }
        //5.释放资源(原则事先创建的后关闭)
        rs.close();
        pstmt.close();
        conn.close();//这里的链接不是真正的关闭，而是连接换回池中。
    }

    @Test
    void testAdd()throws Exception{
        //1.获取连接
        Connection conn=dataSource.getConnection();
        //2.创建Statement(基于此对象发送sql)
        String sql="insert into tb_sys_notices (title,type,content,status,user_id,created_time,modified_time) values (?,?,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1,"title-A");
        pstmt.setString(2, "1");
        pstmt.setString(3, "Content-A");
        pstmt.setString(4, "0");
        pstmt.setInt(5,101);
        pstmt.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
        pstmt.setTimestamp(7, new java.sql.Timestamp(System.currentTimeMillis()));
        //3.发送sql
        pstmt.execute();
        System.out.println("insert ok");
        //4.处理结果(一般查询操作处理结果)
        //5.释放资源
        pstmt.close();
        conn.close();
    }
}
