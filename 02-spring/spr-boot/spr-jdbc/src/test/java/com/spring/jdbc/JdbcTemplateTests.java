package com.spring.jdbc;

import com.spring.pojo.Notice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class JdbcTemplateTests {
    /**
     * jdbcTemplate是Spring官方提供的基于jdbc技术访问数据库的一个API对象。
     * 此对象基于模板方法模式，对jdbc操作进行了封装，我们基于此对象，以更
     * 简单的一个步骤操作数据库中的数据。
     * 说明：在springboot工程中只要你添加了spring-boot-starter-jdbc依赖，
     * 服务启动时，就会构建此对象，所以，你用的时候直接从spring容器去获取即可
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void testAdd(){
        String sql= "insert into tb_sys_notices (title,type,content,status,user_id,created_time,modified_time) values (?,?,?,?,?,?,?)";
        Object[] args={"title-B","1","Content-B","1",102,new Date(),new Date()};
        int update = jdbcTemplate.update(sql, args);
        System.out.println(update);
    }

    @Test
    void testUpdate(){
        String sql="update tb_sys_notices set content=?,status=? where id=?";
        Object[] args={"Content-B",0,2};
        jdbcTemplate.update(sql,args);
    }

    @Test
    void testDelete(){
        String sql="delete from tb_sys_notices where id in (?,?)";
        Object[] args={100,101};
        jdbcTemplate.update(sql,args);
    }

    @Test
    void testQuery01(){
        String sql="select id,title,type,content,status,user_id userId,created_time createdTime " +
                " from tb_sys_notices " +
                " where id=?";//?表示占位符
        Object[] args={1};
        Map<String, Object> map = jdbcTemplate.queryForMap(sql, args);
        System.out.println(map);
    }

    @Test
    void testQuery02(){
        String sql="select id,title,type,content,status,user_id userId,created_time createdTime " +
                " from tb_sys_notices " +
                " limit ?,?";
        Object[] args={0,3};//最多取3条记录
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql,args);
        System.out.println(list);
    }

    @Test
    void testQuery03(){
        String sql="select id,title,type,content,status,user_id userId,created_time createdTime " +
                " from tb_sys_notices " +
                " where id=?";//?表示占位符
        Notice notice = jdbcTemplate.queryForObject(sql,
                        new BeanPropertyRowMapper<>(Notice.class),
                 1);
        System.out.println(notice);
    }

    @Test
    void testQuery04(){
        String sql="select id,title,type,content,status,user_id userId,created_time createdTime " +
                " from tb_sys_notices " +
                " limit ?,?";//?表示占位符
        List<Notice> list = jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(Notice.class),
                0,3);//3表示最多取3条
        System.out.println(list.size());
    }

    /**
     * 课堂练习：基于jdbcTemplate对象实现数据的批量添加
     */
    @Test
    void testBatchAdd(){
        String sql= "insert into tb_sys_notices (title,type,content,status,user_id,created_time,modified_time) values (?,?,?,?,?,?,?)";
        Object[] args1={"title-C","1","Content-C","1",102,new Date(),new Date()};
        Object[] args2={"title-D","1","Content-D","1",102,new Date(),new Date()};
        Object[] args3={"title-E","1","Content-E","1",102,new Date(),new Date()};
        List<Object[]> list=new ArrayList<>();
        list.add(args1);
        list.add(args2);
        list.add(args3);
        jdbcTemplate.batchUpdate(sql, list);
    }

}
