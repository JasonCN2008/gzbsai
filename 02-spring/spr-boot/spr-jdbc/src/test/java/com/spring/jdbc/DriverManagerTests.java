package com.spring.jdbc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SpringBootTest
public class DriverManagerTests {
    @Test
    void testGetConnection() throws SQLException {
        String url="jdbc:mysql://localhost:3306/jy-blog?serverTimezone=Asia/Shanghai&characterEncoding=utf8";
        Connection conn=DriverManager.getConnection(url,"root","root");
        System.out.println(conn);
        conn.close();//直接关闭连接。
    }
}
