package com.java.reflect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface Component{

}

@Component
class DefaultCache{}

public class AnnotationTests {
    public static void main(String[] args) {
        //1.获取类的字节码对象(此对象是在类加载时创建)
        Class<DefaultCache> defaultCacheClass = DefaultCache.class;
        //2.判定类的上面是否有Component注解
        boolean annotationPresent =
                defaultCacheClass.isAnnotationPresent(Component.class);
        System.out.println(annotationPresent);

    }
}
