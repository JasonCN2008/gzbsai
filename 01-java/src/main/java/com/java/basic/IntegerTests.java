package com.java.basic;

/**
 * 整数池的考核
 * 1)考核整数池的概念
 * 2)考核对象的比较
 * 注意：我们可以通过JVM参数改变整数池的大小，
 * 整数池中的最小值不可以改变，最大值可以修改，可以通过配置jvm参数进行实践
 * -XX:AutoBoxCacheMax=你给的值
 */
public class IntegerTests {//IntegerTests.java-->IntegerTests.class
    public static void main(String[] args) {
        Integer a=100; //自动封箱Integer.valueOf(100)，封箱时会从整数池中取100
        Integer b=100;//默认整数池存储的数据为-128~+127
        Integer c=200;
        Integer d=200;
        System.out.println(a==b);//true
        System.out.println(c==d);//false
    }
}
